import pm4py
from pm4py.objects.petri_net.utils.reduction import *
from utils import *

net, im, fm = pm4py.read_pnml('demo.pnml')
log = pm4py.read_xes('demo.xes.gz')

net, im, fm= unblock_IM_model(net, im, fm, log, 
                              tolerance=0.005, 
                              view_final_net=False)
pm4py.write_pnml(net, im, fm, "demo_result.pnml")